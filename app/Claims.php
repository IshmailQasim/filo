<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claims extends Model
{
  protected $fillable = ['Reason'];
}
