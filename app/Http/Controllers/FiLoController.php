<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\FiLo;
use Gate;

class FiLoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $fi_los = FiLo::all()->toArray();
    return view('fi_los.index', compact('fi_los'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('fi_los.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    // form validation
    $lostItem = $this->validate(request(), [
      'category' => 'required',
      'found_time' => 'required',
      'found_place' => 'required',
      'found_user' => 'required',
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
      'description' => 'sometimes',
    ]);
    //Handles the uploading of the image
    if ($request->hasFile('image')){
      //Gets the filename with the extension
      $fileNameWithExt = $request->file('image')->getClientOriginalName();
      //just gets the filename
      $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
      //Just gets the extension
      $extension = $request->file('image')->getClientOriginalExtension();
      //Gets the filename to store
      $fileNameToStore = $filename.'_'.time().'.'.$extension;
      //Uploads the image
      $path =$request->file('image')->storeAs('public/images', $fileNameToStore);
    }
    else {
      $fileNameToStore = 'noimage.jpg';
    }
    // create a LostItem object and set its values from the input
    $lostItem = new FiLo;
    $lostItem->category = $request->input('category');
    $lostItem->found_time = $request->input('found_time');
    $lostItem->found_user = $request->input('found_user');
    $lostItem->found_place = $request->input('found_place');
    $lostItem->colour = $request->input('colour');
    $lostItem->image = $fileNameToStore;
    $lostItem->description = $request->input('description');
    $lostItem->other_information = $request->input('other_information');
    $lostItem->created_at = now();
    // save the LostItem object
    $lostItem->save();
    // generate a redirect HTTP response with a success message
    return redirect('fi_los')->with('success', 'Lost item has been added');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    if(Auth::check())
    {
      $fi_los = FiLo::find($id);
      return view('fi_los.show', compact('fi_los'));
    }
    else
    {
      return redirect()->route('login');
    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $fi_los = FiLo::find($id);
    return view('fi_los.edit',compact('fi_los'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
      $lostItem = FiLo::find($id);
      $this->validate(request(), [
        'category' => 'required',
        'found_time' => 'required',
        'found_place' => 'required',
        'found_user' => 'required'
      ]);

      $lostItem->category = $request->input('category');
      $lostItem->found_time = $request->input('found_time');
      $lostItem->found_user = $request->input('found_user');
      $lostItem->found_place = $request->input('found_place');
      $lostItem->colour = $request->input('colour');
      $lostItem->description = $request->input('description');
      $lostItem->other_information = $request->input('other_information');
      $lostItem->updated_at = now();

      //Handles the uploading of the image
      if ($request->hasFile('image')){
        //Gets the filename with the extension
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        //just gets the filename
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        //Just gets the extension
        $extension = $request->file('image')->getClientOriginalExtension();
        //Gets the filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        //Uploads the image
        $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
      } else {
        $fileNameToStore = 'noimage.jpg';
      }
      $lostItem->image = $fileNameToStore;
      $lostItem->save();

      $userQuery = FiLo::all();
      if (Gate::denies('display'))
      {
        $userQuery=$userQuery->where('userid', auth()->user()->id);
      }
      return view('fi_los.create', array('fi_los'=>$userQuery));
      return redirect('fi_los')->with('success','Item has been updated!');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
      $lostItem = FiLo::find($id);
      $lostItem->delete();
      return redirect('fi_los')->with('success','Item has been deleted');
    }

  }
