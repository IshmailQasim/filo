<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Claims;
use Illuminate\Support\Facades\Auth;


class ClaimsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //checks to see if user is admin and returns index of claims if true.
      if (Auth::check() && Auth::user()->role)
      {
        $claims = Claims::all()->toArray();
        return view('claims.index', compact('claims'));
      }
      else //redirect home
      {
        return redirect()->route('/');
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, FiLo $id)
    {
        $fi_los = FiLo::find($id);
        return view('claims.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $claim = $this->validate(request(), [
        'Reason' => 'required',
      ]);
      // create a claim object and set its values from the input
      $claim = new Claims;
      $claim->userID =auth()->user()->id;
      $claim->itemID = $request->input('itemID');
      $claim->Reason = $request->input('Reason');
      $claim->created_at = now();
      // save the request reason
      $claim->save();
      // generate a redirect HTTP response with a success message
      return redirect('fi_los')->with('Success', 'Request has been added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('claims')->with('success','Request has been approved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $claim = Claims::find($id);
      $claim->delete();
      return redirect('claims')->with('success','Request has been deleted');
    }
}
