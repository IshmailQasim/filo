<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiLo extends Model
{
  protected $fillable = ['found_time', 'found_user', 'found_place',
  'colour', 'description', 'other_information'];
}
