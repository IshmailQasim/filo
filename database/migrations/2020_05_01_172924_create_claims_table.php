<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->id();
            $table->UnsignedBigInteger('userID')->nullable(false);
            $table->UnsignedBigInteger('itemID')->nullable(false);
            //$table->string('item');
            $table->string('Reason');
            $table->timestamps();
            $table->foreign('userID')->references('id')->on('users');
            $table->foreign('itemID')->references('id')->on('fi_los');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
