<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiLosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fi_los', function (Blueprint $table) {
            $table->id();
            $table->enum('category',['pet', 'phone', 'jewellery'])->default('phone');
            $table->time('found_time');
            $table->string('found_user', 30);
            $table->string('found_place', 256)->nullable();
            $table->string('colour', 30)->nullable();
            $table->string('image', 256)->nullable();
            $table->string('description', 256)->nullable();
            $table->string('other_information', 256)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fi_los');
    }
}
