@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8 ">
      <div class="card">
        <div class="card-header">Display All Item Information</div>
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>category</th>
                <th>found_time</th>
                <th>found_user</th>
                <th>found_place</th>
                <th>colour</th>
                <th colspan="2">image</th>
                <th>description</th>
                <th>other_information</th>
                <th>created_at</th>
                <th colspan="3">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$fi_los['category']}}</td>
                <td>{{$fi_los['found_time']}}</td>
                <td>{{$fi_los['found_user']}}</td>
                <td>{{$fi_los['found_place']}}</td>
                <td>{{$fi_los['colour']}}</td>
                <td colspan="2"><img src="{{URL('/storage/images/' . $fi_los->image)}}" style="width:100%; height:auto;"/></td>
                <td>{{$fi_los['description']}}</td>
                <td>{{$fi_los['other_information']}}</td>
                <td>{{$fi_los['created_at']}}</td>

                <td><a href="{{action('FiLoController@edit', $fi_los['id'])}}" class="btn
                  btn- primary">Edit</a></td>

                  <td><a href="{{route('makeClaim', array('id' => $fi_los->id))}}" class="btn
                    btn- primary">Claim</a></td>

                    <!-- <td>
                      <form action="{{action('ClaimsController@create', $fi_los['id'])}}"
                      method="post"> @csrf
                      <input name="_method" type="hidden" value="GET">
                      <button class="btn btn-danger" type="submit"> Claim</button>
                    </form>
                  </td> -->

                  <td>
                    <form action="{{action('FiLoController@destroy', $fi_los['id'])}}"
                    method="post"> @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit"> Delete</button>
                  </form>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
