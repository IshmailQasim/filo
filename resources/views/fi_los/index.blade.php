@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8 ">
      <div class="card">
        <div class="card-header">Display all vehicles</div>
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>category</th>
                <th>colour</th>
                <th>description</th>
                <th>created_at</th>
                <th colspan="3">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($fi_los as $lostItem)
              <tr>
                <td>{{$lostItem['category']}}</td>
                <td>{{$lostItem['colour']}}</td>
                <td>{{$lostItem['description']}}</td>
                <td>{{$lostItem['created_at']}}</td>
                <td><a href="{{action('FiLoController@show', $lostItem['id'])}}" class="btn
                  btn- primary">Details</a></td>
                  <td><a href="{{action('FiLoController@edit', $lostItem['id'])}}" class="btn
                    btn- warning">Edit</a></td>
                    <td>
                      <form action="{{action('FiLoController@destroy', $lostItem['id'])}}"
                      method="post"> @csrf
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn btn-danger" type="submit"> Delete</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
