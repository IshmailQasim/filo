@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8 ">
      <div class="card">
        <div class="card-header">Edit and update the Lost Item</div>
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div><br />
        @endif
        @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="card-body">
          <form class="form-horizontal" method="POST" action="{{ action('FiLoController@update',
          $fi_los['id']) }} " enctype="multipart/form-data" >
          @method('PATCH')
          @csrf

          <div class="col-md-8">
            <label>Category</label>
            <select name="category">
              <option value="Pet">pet</option>
              <option value="Phone">phone</option>
              <option value="Jewellery">jewellery</option>
            </select>
          </div>

          <div class="col-md-8">
            <label>The Time the Item was Found</label>
            <input type="time" name="found_time"
            placeholder = "The Time the Item was Found" />
          </div>

          <div class="col-md-8">
            <label >Who Item Was Found By</label>
            <input type="text" name="found_user"
            placeholder="Who Item Was Found By" />
          </div>

          <div class="col-md-8">
            <label >Location Item was Found</label>
            <input type="text" name="found_place"
            placeholder="Location Item was Found" />
          </div>

          <div class="col-md-8">
            <label >Item Colour</label>
            <input type="text" name="colour"
            placeholder="Item Colour" />
          </div>

          <div class="col-md-8">
            <label>Image</label>
            <input type="file" name="image" placeholder="Image file" />
          </div>

          <div class="col-md-8">
            <label >Description</label>
            <textarea rows="4" cols="50" name="description">
              Notes about the lost item </textarea>
          </div>

          <div class="col-md-8">
            <label >Other Information</label>
            <input type="text" name="other_information"
            placeholder="Other Information" />
          </div>

          <div class="col-md-6 col-md-offset-4">
            <input type="submit" class="btn btn-primary" />
            <input type="reset" class="btn btn-primary" />

          </a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
@endsection
