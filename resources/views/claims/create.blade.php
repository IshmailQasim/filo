<!-- inherit master template app.blade.php -->
@extends('layouts.app')
<!-- define the content section -->
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 ">
      <div class="card">
        <div class="card-header">Request your Item</div>
        <!-- display the errors -->
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <!-- define the form -->

        <div class="container-fluid">

          {!! Form::open(['action' => array('ClaimsController@store','FiLo'=>$id), 'method'=>'POST']) !!}
          @csrf
          <div class="form-group row">

              {{ Form::label('Reason for Request','Reason',['class'=>"col-form-label"]) }}
              <div class="col-sm-10 ml-1">
                  {{ Form::textarea   ('Reason','',['class'=>"form-control"]) }}
              </div>
          </div>
          <div  class="form-group row justify-content-end">
              {{ Form::submit('Request',['class'=>'btn btn-success']) }}
          </div>

          {!! Form::close() !!}

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
