@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8 ">
      <div class="card">
        <div class="card-header">Display all vehicles</div>
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>UserID</th>
                <th>item</th>
                <th>Reason</th>
                <th>created_at</th>
                <th colspan="3">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($claims as $claim)
              <tr>
                <td>{{$claim['UserID']}}</td>
                <td>{{$claim['item']}}</td>
                <td>{{$claim['Reason']}}</td>
                <td>{{$claim['created_at']}}</td>

                <!-- <td>
                  <form action="{{action('FiLoController@returnLost', $fi_los['id'])}}"
                  method="post"> @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit"> Delete</button>
                </form>
                </td> -->
                      <form action="{{action('FiLoController@destroy', $lostItem['id'])}}"
                      method="post"> @csrf
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn btn-danger" type="submit"> Refuse</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
