<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();


Route::get('claims/create/{id}', function($id)
{
  if (Auth::check())
  {
    return view('claims.create',['id' => $id]);
  }
  else
  {
    return redirect()->route('login');
  }
})->name('makeClaim');


Route::get('/home', 'HomeController@index');


Route::resource('fi_los', 'FiLoController');


Route::resource('claims','ClaimsController');
